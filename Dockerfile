FROM ubuntu:18.04

RUN useradd -ms /bin/bash -g root -G sudo user1

RUN apt-get update
RUN apt-get install python3 -y

USER user1
WORKDIR /home/user1
COPY hello_world.py .

COPY index.html .

RUN echo '#!/bin/bash' > background_run.sh
RUN echo 'python3 -m http.server 8082' >> background_run.sh
RUN echo 'sleep infinity' >> background_run.sh
RUN chmod +x background_run.sh

EXPOSE 8082

CMD /home/user1/background_run.sh
