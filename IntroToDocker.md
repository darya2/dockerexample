# Introduction

1. What is docker?

Docker is a service which can be used to run and deploy applications within a specific environment. For example, within a Windows machine, we can use docker to run a Python Flask application within in a Ubuntu OS environment. More details here: https://docs.docker.com/get-started/overview/

2. How to check if the docker daemon (the background service) is running? `ps aux | grep docker`

3. What is a container? `docker ps -a`

A docker container is the lightweight software that actually runs your code within the built specific environment. The above command lists all the containers (whether currently running or not) and their details. More details here: https://www.docker.com/resources/what-container/

4. What is an image? `docker images`

A docker image is a snapshot of built application(s) within the specific environment. Users (ourselves) build an image by specifying the details of the environment and application, including installation, version details, etc. via a *Dockerfile* (see below). A docker container runs an image and thus the application within the image. More details here: https://docs.docker.com/get-started/overview/#images

5. What is a Dockerfile?

The Dockerfile is the set of instructions given to Docker to build an image containing the necessary environment (including OS, libraries, env variables, etc.) and the target application to be run (including installation of the application, files/database required, how to run the application, etc.).

# Run the Getting Started image

`docker run -d -p 80:8080 docker/getting-started`

1. What is `docker/getting-started`?

`docker/getting-started` is a ready-made image hosted by docker: https://hub.docker.com/r/docker/getting-started. 

2. What does `docker run` do?

`docker run` first checks for the image, in this case, `docker/getting-started` locally. If it does not find an image named this way, it accesses hub.docker.com and retrieves the image from there (provided there is one with that name). It then uses the remaining arguments (`-d -p 80:80`) to "run" the image.

3. What are `-d` and `-p`? We can even aggregate options as `-dp`.

`-d` means to run the container in the background, i.e. as a **d**aemon. `-p` means the port to run it on.

4. What does 80:8080 mean? Lets go to `localhost:80` in our browsers

`-p 80:8080` means to bind the port 8080 of the docker container to the port 80 on the local machine. So, whatever is running in port 8080 within the docker container, will be accessible via port 80 on the local machine (localhost or 127.0.0.1)

5. Seeing the current container and images

`docker ps` shows you the list of *running* containers.

`docker images` shows you the list of images.

6. Stopping the container `docker stop <container_id>`

When you are done with running your application, stop the container (otherwise it will continue to use the machine's resources).

# How to build your own Container

## Creating a Dockerfile
  1. the "FROM" keyword
  2. the "RUN" keyword
  3. the "USER" keyword
  4. the "WORKDIR" keyword
  5. the "COPY" keyword
  6. the "CMD" keyword
  7. the "EXPOSE" keyword

## Building the image:

`docker build -t "helloworld:1.0" .`

## Running the container:

### Foreground:

`docker run -it -p 8082:8082 helloworld:1.0 /bin/bash`

### Background:

`docker run -d -p 8082:8082 helloworld:1.0`

# Debugging Docker containers

`docker logs <CONTAINER_ID>`

## Stopping the Docker Container

`docker stop <CONTAINER_ID>`

## Restart the Docker Container

`docker start <CONTAINER_ID>`

## Enter the bash shell of a foreground container that has been restarted

`docker attach <CONTAINER_ID>`

## Cleaning up Docker containers and images:

1. To check which containers are still running: `docker ps`
2. To check all containers that exist on the machine: `docker ps -a`
3. Find the “Container ID” that you want to stop / remove
4. If the container is still running, stop the container: `docker stop <CONTAINER_ID>`
5. Once the container has stopped (may take a few seconds), then remove the container: `docker rm <CONTAINER_ID>`
6. Find the image that you no longer need: `docker images`
7. Locate the “Image ID”, and remove the image: `docker rmi <IMAGE_ID>`
8. Verify the container has been deleted using `docker ps -a` and the image no longer exists using `docker images`
