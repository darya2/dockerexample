# Installation

https://docs.docker.com/engine/install/

# Introduction

1. What is docker?
2. What is a container? `docker ps -a`
3. What is an image? `docker images`
4. How to check if the docker daemon (the background service) is running? `ps aux | grep docker`
5. What is a Dockerfile?

# Run the Getting Started image

`docker run -d -p 80:80 docker/getting-started`

1. What does `docker run` do?
2. What is `docker/getting-started`?
3. What are `-d` and `-p`? We can even aggregate options as `-dp`.
4. What does 80:80 mean? Lets go to `localhost:80` in our browsers
5. Seeing the current container and images
6. Stopping the container `docker stop <container_id>`

# How to build your own Container

## Creating a Dockerfile
  1. the "FROM" keyword
  2. the "RUN" keyword
  3. the "USER" keyword
  4. the "WORKDIR" keyword
  5. the "COPY" keyword
  6. the "CMD" keyword
  7. the "EXPOSE" keyword

## Building the image:

`docker build -t "helloworld:1.0" .`

## Running the container:

### Foreground:

`docker run -it -p 8082:8082 helloworld:1.0 /bin/bash`

### Background:

`docker run -d -p 8082:8082 helloworld:1.0`

# Debugging Docker containers

`docker logs <CONTAINER_ID>`

## Stopping the Docker Container

`docker stop <CONTAINER_ID>`

## Restart the Docker Container

`docker start <CONTAINER_ID>`

## Enter the bash shell of a foreground container that has been restarted

`docker attach <CONTAINER_ID>`

## Cleaning up Docker containers and images:

1. To check which containers are still running: `docker ps`
2. To check all containers that exist on the machine: `docker ps -a`
3. Find the “Container ID” that you want to stop / remove
4. If the container is still running, stop the container: `docker stop <CONTAINER_ID>`
5. Once the container has stopped (may take a few seconds), then remove the container: `docker rm <CONTAINER_ID>`
6. Find the image that you no longer need: `docker images`
7. Locate the “Image ID”, and remove the image: `docker rmi <IMAGE_ID>`
8. Verify the container has been deleted using `docker ps -a` and the image no longer exists using `docker images`
